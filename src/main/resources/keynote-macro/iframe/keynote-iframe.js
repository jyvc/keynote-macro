function deParams(str) {
	return (str || document.location.search)
		.replace(/(^\?)/, '')
		.split("&")
		.map(function (n) {
				return n = n.split("="), this[n[0]] = decodeURIComponent(n[1]), this
			}.bind({})
		)[0];
}

function updateCssTheme(theme) {
	if (theme && theme !== 'beige') {
		var link = (document.querySelector('#css-theme') || {href: ''});
		link.href = "reveal.js/theme/" + theme + ".css";
	}
}

function extractData(pageTitle, $mainContent) {
	var data = {
		s: [{
			c: pageTitle ? ('<h1>' + pageTitle + '</h1>') : null
		}]
	};

	function addSlides($container, innerData) {
		$container.find('>.keynote').each(function () {
			var $slide = $(this);
			var $html = $('<div>' + $slide.html().replace((/[\n\s]+/g), ' ') + '</div>');
			var $speakerNotes = $slide.find('>.keynote-speakerNotes>*');
			var speaker = ($('<div/>').append($speakerNotes.clone()).html() || '').replace((/[\n\s]+/g), ' ');
			$html.find('.keynote,.keynote-speakerNotes').remove();
			var content = $html.html();
			var slideData = {};
			slideData.t = $slide.data('transition');
			slideData.h = $slide.data('theme');
			if (content && content.length > 0) {
				slideData.c = content;
			}
			if (speaker && speaker.length > 0) {
				slideData.n = speaker;
			}
			addSlides($slide, slideData);
			if (!innerData.s) {
				innerData.s = [];
			}
			innerData.s.push(slideData);
		});
		innerData.t = $container.data('transition');
		innerData.h = $container.data('theme');
	}

	addSlides($mainContent, data);
	data.transition = $mainContent.data('transition');
	return data;
}

function addSlideInto(containerDom, slideData) {
	var hasInnerSlides = (slideData.s && slideData.s.length && slideData.s.length > 0);
	var speaker = slideData.n ? ('<aside class="notes">' + slideData.n + '</aside>') : '';
	var content = (slideData.c || '') + speaker;
	console.log("add slide", containerDom, slideData, hasInnerSlides, speaker, content);
	var sectionDom;

	if (slideData.c || slideData.n) {
		var html = hasInnerSlides ? ('<section>' + content + '</section>') : content;
		sectionDom = document.createElement('section');
		if (slideData.t) {
			sectionDom.dataset.transition = slideData.t;
		}
		if (slideData.h) {
			sectionDom.dataset.theme = slideData.h;
		}
		sectionDom.innerHTML = html || '';
		containerDom.appendChild(sectionDom);
	} else {
		sectionDom = containerDom;
	}


	if (hasInnerSlides) {
		slideData.s.forEach(function (innerSlideData) {
			addSlideInto(sectionDom, innerSlideData);
		});
	}
}

var slidesDom = document.querySelector('.slides');
var $loader = $('#loader');

function updateKeynote(data) {
	$loader.remove();
	slidesDom.innerHTML = '';
	addSlideInto(slidesDom, data);

	var h1 = document.querySelector('h1');
	if (h1) {
		document.title = h1.innerText;
	}

	var corePluginUrlPrefix = "reveal.js/";
	var customPluginUrlPrefix = "reveal.js-custom-plugins/";
	// More info about config & dependencies:
	// - https://github.com/hakimel/reveal.js#configuration
	// - https://github.com/hakimel/reveal.js#dependencies
	Reveal.initialize({
		plugins: [RevealMarkdown, RevealHighlight, RevealNotes, RevealMenu],
		// Display the page number of the current slide
		slideNumber: true,
		// Add the current slide number to the URL hash so that reloading the
		// page/copying the URL will return you to the same slide
		hash: true,
		// Push each slide change to the browser history. Implies `hash: true`
		history: false,

		// See https://github.com/hakimel/reveal.js/#navigation-mode
		navigationMode: data.navigationMode || 'default',

		// Transition style
		transition: data.transition || 'slide', // none/fade/slide/convex/concave/zoom
		menu: {
			// Specifies which side of the presentation the menu will
			// be shown. Use 'left' or 'right'.
			side: 'left',

			// Specifies the width of the menu.
			// Can be one of the following:
			// 'normal', 'wide', 'third', 'half', 'full', or
			// any valid css length value
			width: 'normal',

			// Add slide numbers to the titles in the slide list.
			// Use 'true' or format string (same as reveal.js slide numbers)
			numbers: true,

			// Specifies which slide elements will be used for generating
			// the slide titles in the menu. The default selects the first
			// heading element found in the slide, but you can specify any
			// valid css selector and the text from the first matching
			// element will be used.
			// Note: that a section data-menu-title attribute or an element
			// with a menu-title class will take precedence over this option
			titleSelector: 'h1, h2, h3, h4, h5, h6',

			// If slides do not have a matching title, attempt to use the
			// start of the text content as the title instead
			useTextContentForMissingTitles: false,

			// Hide slides from the menu that do not have a title.
			// Set to 'true' to only list slides with titles.
			hideMissingTitles: false,

			// Adds markers to the slide titles to indicate the
			// progress through the presentation. Set to 'false'
			// to hide the markers.
			markers: true,

			// Specify custom panels to be included in the menu, by
			// providing an array of objects with 'title', 'icon'
			// properties, and either a 'src' or 'content' property.
			custom: [
				{
					title: 'Actions',
					icon: '<i class="fas fa-random"></i>',
					content: '<ul class="menu-actions">' +
						'<li class="action-fullscreen"><button><i class="fas fa-tv"></i> <span class="label">Fullscreen</span></button></li>' +
						'<li class="action-overview"><button><i class="fas fa-th-large"></i> <span class="label">Overview</span></button></li>' +
						'<li class="action-speakernotes"><button><i class="fas fa-comment"></i> <span class="label">Speaker notes</span></button></li>' +
						'<li class="action-pause"><button><i class="fas fa-power-off"></i> <span class="label">pause</span></button></li>' +
						'</ul>'
				}
			],

			// Specifies the themes that will be available in the themes
			// menu panel. Set to 'true' to show the themes menu panel
			// with the default themes list. Alternatively, provide an
			// array to specify the themes to make available in the
			// themes menu panel, for example...
			//
			// [
			//     { name: 'Black', theme: 'dist/theme/black.css' },
			//     { name: 'White', theme: 'dist/theme/white.css' },
			//     { name: 'League', theme: 'dist/theme/league.css' },
			//     {
			//       name: 'Dark',
			//       theme: 'lib/reveal.js/dist/theme/black.css',
			//       highlightTheme: 'lib/reveal.js/plugin/highlight/monokai.css'
			//     },
			//     {
			//       name: 'Code: Zenburn',
			//       highlightTheme: 'lib/reveal.js/plugin/highlight/zenburn.css'
			//     }
			// ]
			//
			// Note: specifying highlightTheme without a theme will
			// change the code highlight theme while leaving the
			// presentation theme unchanged.
			themes: false,

			// Specifies the path to the default theme files. If your
			// presentation uses a different path to the standard reveal
			// layout then you need to provide this option, but only
			// when 'themes' is set to 'true'. If you provide your own
			// list of themes or 'themes' is set to 'false' the
			// 'themesPath' option is ignored.
			themesPath: 'dist/theme/',

			// Specifies if the transitions menu panel will be shown.
			// Set to 'true' to show the transitions menu panel with
			// the default transitions list. Alternatively, provide an
			// array to specify the transitions to make available in
			// the transitions panel, for example...
			// ['None', 'Fade', 'Slide']
			transitions: false,

			// Adds a menu button to the slides to open the menu panel.
			// Set to 'false' to hide the button.
			openButton: true,

			// If 'true' allows the slide number in the presentation to
			// open the menu panel. The reveal.js slideNumber option must
			// be displayed for this to take effect
			openSlideNumber: false,

			// If true allows the user to open and navigate the menu using
			// the keyboard. Standard keyboard interaction with reveal
			// will be disabled while the menu is open.
			/* false to allow actions menus */
			keyboard: false,

			// Normally the menu will close on user actions such as
			// selecting a menu item, or clicking the presentation area.
			// If 'true', the sticky option will leave the menu open
			// until it is explicitly closed, that is, using the close
			// button or pressing the ESC or m key (when the keyboard
			// interaction option is enabled).
			sticky: false,

			// If 'true' standard menu items will be automatically opened
			// when navigating using the keyboard. Note: this only takes
			// effect when both the 'keyboard' and 'sticky' options are enabled.
			autoOpen: false,

			// If 'true' the menu will not be created until it is explicitly
			// requested by calling RevealMenu.init(). Note this will delay
			// the creation of all menu panels, including custom panels, and
			// the menu button.
			delayInit: false,

			// If 'true' the menu will be shown when the menu is initialised.
			openOnInit: false,

			// By default the menu will load it's own font-awesome library
			// icons. If your presentation needs to load a different
			// font-awesome library the 'loadIcons' option can be set to false
			// and the menu will not attempt to load the font-awesome library.
			loadIcons: true
		}
	});
}


function updateContentFromHtml(title, html) {
	var data = extractData(title, $('<div>' + html + '</div>'));
	console.log("Receive data:", data);
	updateKeynote(data);
}

/**
 * @param contentUrl URL of content. For example: https://confluence-server/confluence-context/rest/api/content/1234567890?expand=body.view
 */
function fetchPageContent(contentUrl) {
	if (!contentUrl) {
		throw "Content URL not defined.";
	}
	console.log("Load data from:", contentUrl);
	$.getJSON(contentUrl)
		.done(function (data) {
			console.log("Data loaded:", data);
			var metadata = (data && data._links || {});
			var htmlPageUrl = metadata.base + metadata.webui;
			$.ajax({
				url: htmlPageUrl,
				dataType: "html"
			})
				.done(function (responseText) {
					$($.parseHTML(responseText))
						.filter('link[href$="/_/styles/custom.css"],link[href$="/contextbatch/css/_super/batch.css"]')
						.first()
						.appendTo('head')
					;
				})
				.fail(function (jqXHR, textStatus, errorThrown) {
					console.error("Unable to load HTML page: ", htmlPageUrl, jqXHR.statusText || errorThrown || textStatus);
				});
			if (data && data.body && data.body.view && data.body.view.value) {
				updateContentFromHtml(data.title, data.body.view.value);
			} else {
				throw "Body is null for content:" + contentUrl;
			}
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			throw "Unable to load content: " + contentUrl + " (" + (jqXHR.statusText || errorThrown || textStatus) + ")";
		});
}


function main() {
	window.onerror = function (messageOrEvent, source, noligne, nocolonne, error) {
		$loader.remove();
		$('<div id="error"></div>').text(getMessage(error) || getMessage(messageOrEvent) || "Unable to render keynote").appendTo('body');
		console.error("Unable to update slide content:", messageOrEvent, " on: ", source + "#line=" + noligne);
	};

	var params = deParams();
	console.log("Page params:", params, window, window.document.location.href, window.document.body.innerHTML);
	var theme = (params.theme || '').replace((/[^a-z]/), '');
	var contentUrl = (params.contentUrl || '').replace((/^[^\/:]+:\/\/[^\/]+/), '');
	updateCssTheme(theme);
	fetchPageContent(contentUrl);

	$(document)
		.on('click', '.action-fullscreen button', function () {
			document.dispatchEvent(new KeyboardEvent(
				'keydown',
				{
					altKey: false,
					bubbles: true,
					cancelable: true,
					code: "keyF",
					composed: true,
					ctrlKey: false,
					detail: 0,
					isComposing: false,
					key: 'f',
					keyCode: 70,
					location: 0,
					metaKey: false,
					repeat: false,
					shiftKey: false,
					view: window
				}
			));
		})
		.on('click', '.action-overview button', function () {
			Reveal.toggleOverview();
		})
		.on('click', '.action-speakernotes button', function () {
			document.dispatchEvent(new KeyboardEvent('keydown', {
				altKey: false,
				bubbles: true,
				cancelable: true,
				code: "keyS",
				composed: true,
				ctrlKey: false,
				detail: 0,
				isComposing: false,
				key: 's',
				keyCode: 83,
				location: 0,
				metaKey: false,
				repeat: false,
				shiftKey: false,
				view: window
			}));
		})
		.on('click', '.action-pause button', function () {
			Reveal.togglePause();
		})
	;
}

function getMessage(messageOrEvent) {
	if (typeof messageOrEvent === "string") {
		return messageOrEvent;
	}
	return messageOrEvent.message;
}

main();
