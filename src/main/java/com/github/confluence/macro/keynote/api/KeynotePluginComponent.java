package com.github.confluence.macro.keynote.api;

public interface KeynotePluginComponent
{
    String getName();
}
