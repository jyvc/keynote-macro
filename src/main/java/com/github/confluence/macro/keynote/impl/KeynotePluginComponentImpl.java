package com.github.confluence.macro.keynote.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.github.confluence.macro.keynote.api.KeynotePluginComponent;

import javax.inject.Inject;
import javax.inject.Named;

@ExportAsService({KeynotePluginComponent.class})
@Named("keynotePluginComponent")
public class KeynotePluginComponentImpl implements KeynotePluginComponent {
	@ComponentImport
	private final ApplicationProperties applicationProperties;

	@Inject
	public KeynotePluginComponentImpl(final ApplicationProperties applicationProperties) {
		this.applicationProperties = applicationProperties;
	}

	public String getName() {
		if (null != applicationProperties) {
			return "keynote:" + applicationProperties.getDisplayName();
		}

		return "keynote";
	}
}
