package ut.com.github.confluence.macro.keynote;

import com.github.confluence.macro.keynote.api.KeynotePluginComponent;
import com.github.confluence.macro.keynote.impl.KeynotePluginComponentImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KeynotePluginComponentUnitTest {
	@Test
	public void testMyName() {
		KeynotePluginComponent component = new KeynotePluginComponentImpl(null);
		assertEquals(
				"names must match",
				"keynote",
				component.getName()
		);
	}
}
