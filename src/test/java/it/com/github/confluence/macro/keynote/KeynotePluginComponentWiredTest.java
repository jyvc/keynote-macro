package it.com.github.confluence.macro.keynote;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import com.github.confluence.macro.keynote.api.KeynotePluginComponent;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class KeynotePluginComponentWiredTest {
	private final ApplicationProperties applicationProperties;
	private final KeynotePluginComponent keynotePluginComponent;

	public KeynotePluginComponentWiredTest(ApplicationProperties applicationProperties, KeynotePluginComponent keynotePluginComponent) {
		this.applicationProperties = applicationProperties;
		this.keynotePluginComponent = keynotePluginComponent;
	}

	@Test
	public void testMyName() {
		assertEquals(
				"names must match",
				"keynote:" + applicationProperties.getDisplayName(),
				keynotePluginComponent.getName()
		);
	}
}
